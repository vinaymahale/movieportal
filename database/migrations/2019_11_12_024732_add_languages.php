<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLanguages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('languages')->insert(
            array('name'=>'English')
        );

        DB::table('languages')->insert(
            array('name'=>'Hindi')
        );

        DB::table('languages')->insert(
            array('name'=>'Konkani')
        );

        DB::table('languages')->insert(
            array('name'=>'Marathi')
        );

        DB::table('languages')->insert(
            array('name'=>'Kannada')
        );

        DB::table('languages')->insert(
            array('name'=>'Telugu')
        );

        DB::table('languages')->insert(
            array('name'=>'Malayalam')
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('languages')->where('name','=','English')->delete();
        DB::table('languages')->where('name','=','Hindi')->delete();
        DB::table('languages')->where('name','=','Konkani')->delete();
        DB::table('languages')->where('name','=','Marathi')->delete();
        DB::table('languages')->where('name','=','Kannada')->delete();
        DB::table('languages')->where('name','=','Telugu')->delete();
        DB::table('languages')->where('name','=','Malayalam')->delete();
    }
}
