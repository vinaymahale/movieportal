<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGenres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('genres')->insert(
            array('name'=>'Drama')
        );

        DB::table('genres')->insert(
            array('name'=>'Romance')
        );
        
        DB::table('genres')->insert(
            array('name'=>'Action')
        );

        DB::table('genres')->insert(
            array('name'=>'Comedy')
        );
        
        DB::table('genres')->insert(
            array('name'=>'Thriller')
        );
        
        DB::table('genres')->insert(
            array('name'=>'Horror')
        );
        
        DB::table('genres')->insert(
            array('name'=>'Sci-Fi')
        );
        
        DB::table('genres')->insert(
            array('name'=>'Adventure')
        );
        
        DB::table('genres')->insert(
            array('name'=>'Documentary')
        );
        
        DB::table('genres')->insert(
            array('name'=>'Fantasy')
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('genres')->where('name','=','Drama')->delete();
        DB::table('genres')->where('name','=','Romance')->delete();
        DB::table('genres')->where('name','=','Action')->delete();
        DB::table('genres')->where('name','=','Comedy')->delete();
        DB::table('genres')->where('name','=','Thriller')->delete();
        DB::table('genres')->where('name','=','Horror')->delete();
        DB::table('genres')->where('name','=','Sci-Fi')->delete();
        DB::table('genres')->where('name','=','Adventure')->delete();
        DB::table('genres')->where('name','=','Documentary')->delete();
        DB::table('genres')->where('name','=','Fantasy')->delete();
    }
}
