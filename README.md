Movie Portal web application

Built using Laravel Framework

Database used : Mysql

Functionalities provided:

Admin can add, delete movies and also search movies using "filter by language" or "filter by genre"


Visitors get to see a list of movies with pagination after 5 movies including the featured image.


Visitors can also search movies using "filter by language" or "filter by genre"


On the addition of a movie, a Email and SMS notification is sent to the admin on his/her email-id and mobile number provided at the time of registration 
