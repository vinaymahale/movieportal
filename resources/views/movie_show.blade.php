@extends('layouts.master')

@section('content')
<br>

<div class="container">
    <br>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <table class="table">
    <tbody>

        <tr>
            <td>Movie : <strong>{{ $movie->title }}</strong></td>
        </tr>

        <tr>
            <td><img src="{{url('uploads/movie_posters/'.$movie->poster)}}" class="img-responsive" alt="{{$movie->poster}}"></td>
        </tr>

        <tr>
        <td>
            @if($language)
            Language : {{ $language }}
            @else
                @auth
                    <label for="language">Specify Language :  &nbsp; </label>
                        <form class="form" action="{{route('language.store')}}" method="POST">
                        @csrf
                            <input type="hidden" name="movie_id" value="{{ $movie->id}}">
                            <div class="form-group">
                                <select class="form-control" name="language_id" id="dropdown-menu">
                                @foreach($languages as $languages)
                                    <option value="{{$languages->id}}">{{ $languages->name }}</option>
                                @endforeach    
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">ADD LANGUAGE</button>
                            </div>
                        </form>
                @endauth
                @guest
                    Language : N/A
                @endguest
            @endif
        </td>
        </tr>

        <tr>
        <td>
            @if(count($genres)>0)
                <ul class="list-inline">
                    <label for="genre">Genre :  &nbsp; </label>
                    @foreach($genres as $genre)
                    <li class="list-inline-item">{{$genre->name}}</li>
                    @endforeach
                </ul> 
            @else
                @auth
                    <label for="genre">Genre :  &nbsp; </label>
                    <form class="form" action="{{route('movie_genres.store')}}" method="POST">
                    @csrf
                        <input type="hidden" name="movie_id" value="{{ $movie->id}}">
                        <div class="form-group">
                            <div class="checkbox">    
                                @foreach($tag as $tag)
                                <label><input type="checkbox" name="tag[]" value="{{$tag->id}}"> {{$tag->name}}</label>&nbsp;  
                                @endforeach
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">TAG GENRE</button>
                        </div>
                    </form>
                @endauth
                @guest
                    <label for="genre">Genre : N/A </label>
                @endguest             
            @endif
        </td>
        </tr>

        <tr>
        <td>Description : {{ $movie->description}}</td>
        </tr>
    </tbody>
    </table>

   
</div>
@endsection
