@extends('layouts.master')

@section('content')

<br/>
<div class="container">

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form class="form" action="{{route('movie.store')}}" method="POST" enctype="multipart/form-data">
@csrf
  <div class="form-group"> 
    <label for="title" class="form-label"><strong>Movie Name :</strong></label>
    <input type="text" class="form-control" name="title" placeholder="Add Movie">
  </div>

  <div class="form-group">
    <label for="description" class="form-label"><strong>Description of the Movie :</strong> </label>
    <textarea class="form-control rounded-0" name="description" rows="8"></textarea>
  </div>

  <div class="form-group">
    <label for="release_date" class="form-label"><strong>Release Date</strong></label>
    <div class="form-group input-group date">
    <input type="date" name="release_date" class="form-control" />               
    <span class="glyphicon glyphicon-calender"></span>
    </div>
  </div>

  <div class="form-group">
  <label for="poster" class="form-label"><strong>Upload Poster</strong></label>
    <input type="file" class="form-control-file" name="poster">
  </div>
 
  <div class="form-group">
  <button type="submit" class="btn btn-primary">Submit</button>
  </div>

</form>

</div>
@endsection
