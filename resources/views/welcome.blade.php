@extends('layouts.master')

@section('content')
<br>
<div class="container">
    <div class="row">
    <div class="col-sm-6">
        <div class="card">
        <div class="card-body">
            <label for="language">Search by Language :  &nbsp; </label>
                <form class="form" action="{{route('movie.search')}}" method="POST">
                @csrf
                <input type="hidden" class="form-control" name="language" value="language">
                    <div class="form-group">
                        <select class="form-control" name="language_id" id="dropdown-menu">
                        @foreach($languages as $languages)
                            <option value="{{$languages->id}}">{{ $languages->name }}</option>
                        @endforeach    
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">SEARCH</button>
                    </div>
                </form>
        </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="card">
        <div class="card-body">
            <label for="genre">Search by Genre :  &nbsp; </label>
                <form class="form" action="{{route('movie.search')}}" method="POST">
                @csrf
                <input type="hidden" class="form-control" name="genre" value="genre">
                    <div class="form-group">
                        <select class="form-control" name="genre_id" id="dropdown-menu">
                        @foreach($genres as $genre)
                            <option value="{{$genre->id}}">{{$genre->name}}</option>
                        @endforeach    
                        </select>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">SEARCH</button>
                    </div>
                </form>
        </div>
        </div>
    </div>
    </div>
</div>


<div class="container">
<br>
<table class="table">

<tbody>
@if(count($movies)>0)
@foreach($movies as $movie)
<tr>
<td>Movie : <a href="{{route('movie',$movie->id)}}"><strong>{{ $movie->title}}</strong></a> <br>
Release Date : {{ $movie->release_date->format('d M Y') }} <br><br>
<img src="{{url('uploads/movie_posters/'.$movie->poster)}}" style="float:left;object-fit:cover;" class="img-responsive" alt="{{$movie->poster}}">
</td>
</tr>
@endforeach
@else
<tr>
<td colspan=2>No movies found</td>
</tr>
@endif
</tbody>
</table>

{{ $movies->links() }}
</div>

@endsection
