<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie_language extends Model
{

    public function movies()
    {
    	return $this->belongsTo('App\Movie');
    }

}
