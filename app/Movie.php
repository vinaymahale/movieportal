<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $dates = ['release_date'];
    
    public function movie_genres()
    {
        return $this->hasMany('App\Movie_genre');
    }

    public function movie_languages()
    {
        return $this->hasOne('App\Movie_language');
    }

    
}
