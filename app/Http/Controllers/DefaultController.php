<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Movie;
use App\Movie_language;
use App\Language;
use App\Genre;

use Illuminate\Support\Facades\DB;

class DefaultController extends Controller
{
    public function index(Request $request)
    {      
      $movies=Movie::orderBy('release_date','desc')->paginate(5);      

      $languages = Language::all();
      $genres = Genre::all();
      return view('welcome',['movies'=>$movies,'languages'=>$languages,'genres'=>$genres]);
    }

    public function show($movie)
    {
      $movie = Movie::find($movie);  
      $language_id = Movie_language::where('movie_id',$movie->id)->value('language_id');
      $language = Language::where('id', $language_id)->value('name');
      $id=$movie->id;
      $genres=Genre::whereIn('id',function ($query) use($id) {
              $query->select('genre_id')->from('movie_genres')->Where('movie_id','=',$id);
      })->get();
    
      return view('movie_show',['movie'=>$movie,'language'=>$language,'genres'=>$genres]);
    }



}
