<?php

namespace App\Http\Controllers;

use App\Movie;
use App\Language;
use App\Genre;
use Auth;


use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {      
        if ($request->has('genre')) {
            $id=$request->genre_id;
            $movies=Movie::whereIn('id',function ($query) use($id) {
              $query->select('movie_id')->from('movie_genres')->Where('genre_id','=',$id);
            })->orderBy('release_date','desc')->paginate(5);
            $languages = Language::all();
            $genres = Genre::all();

            if (Auth::user()) {   // Check is user logged in
              return view('home',['movies'=>$movies,'languages'=>$languages,'genres'=>$genres]);
            }
            else{
            return view('welcome',['movies'=>$movies,'languages'=>$languages,'genres'=>$genres]);
            }
        }

        if ($request->has('language')) {
          $id=$request->language_id;
          $movies=Movie::whereIn('id',function ($query) use($id) {
            $query->select('movie_id')->from('movie_languages')->Where('language_id',$id);
          })->paginate(5);
          
          $languages = Language::all();
          $genres = Genre::all();
          
          if (Auth::user()) {   // Check is user logged in
            return view('home',['movies'=>$movies,'languages'=>$languages,'genres'=>$genres]);
          }
          else{
          return view('welcome',['movies'=>$movies,'languages'=>$languages,'genres'=>$genres]);
          }

        }

    }

}
