<?php

namespace App\Http\Controllers;

use App\Movie;
use App\Language;
use App\Movie_language;
use App\Genre;
use Auth;
use Mail;
use File;

use Illuminate\Http\Request;
use App\Http\Requests\MovieStoreRequest;
use Illuminate\Support\Facades\DB;

class MovieController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {         
        return view('movie_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MovieStoreRequest $request)
    {
       $movie=new Movie();
       $movie->title=$request->title;
       $movie->description=$request->description;
       $movie->release_date=$request->release_date;

       if($request->file('poster')){
           $file=$request->file('poster');
           $extension=$file->getClientOriginalExtension();
           $filename=time().'.'.$extension;
           $file->move('uploads/movie_posters/',$filename);
           $movie->poster=$filename;
       }else{
           $movie->poster='';
       }
       $movie->save();

       $email_id=Auth::user()->email;
       $user=Auth::user()->name;
       $contact=Auth::user()->contact_number;
 
       $to_name = $user;
       $to_email = $email_id;
       $data = ['name'=>$user, "movie" => $request->title];
    
       Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
        $message->to($to_email, $to_name)
                ->subject('New Movie Added');
        $message->from('vinaymahale0497@gmail.com','Movie Portal');
        });

        
        $apiKey = urlencode('uWm5yMFCFSo-iS8vItsEnbuNAiFxbizTEhRBCcDeB9');
        
        // Message details
        $number = $contact;
        $sender = urlencode('TXTLCL');
        $message = rawurlencode('Thank you. Movie has been successfully added to our MoviePortal');
    
    
        // Prepare data for POST request
        $data = array('apikey' => $apiKey, 'numbers' => $number, "sender" => $sender, "message" => $message);
    
        // Send the POST request with cURL
        $ch = curl_init('https://api.textlocal.in/send/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        
        // Process your response here
        //echo $response;
        

    return redirect('/home')->with('success','Movie Added successfully');

    
    }

 
    public function show(Movie $movie)
    {

        $movie = Movie::find($movie->id);  
        $language_id = Movie_language::where('movie_id',$movie->id)->value('language_id');
        $language = Language::where('id', $language_id)->value('name');
        $id=$movie->id;
        $genres=Genre::whereIn('id',function ($query) use($id) {
                $query->select('genre_id')->from('movie_genres')->Where('movie_id','=',$id);
        })->get();

        $languages = Language::all();        
        $tag = Genre::all();        

     
        return view('movie_show',['movie'=>$movie,'language'=>$language,'languages'=>$languages,'genres'=>$genres,'tag'=>$tag]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie $movie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie $movie)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movie  $movie
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie $movie)
    {
        $movie = Movie::find($movie->id);
        $movie->movie_genres()->delete();
        $movie->movie_languages()->delete();
        $image_path = "/uploads/movie_posters/".$movie->poster;
       // echo $image_path;
        if(File::exists(public_path($image_path))){
            File::delete(public_path($image_path));  
        }
        $movie->delete();
        return redirect('/home')->with('success','Movie Deleted successfully');
    }
}
