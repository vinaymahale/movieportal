<?php

namespace App\Http\Controllers;

use App\Movie;
use App\Language;
use App\Genre;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $movies=Movie::orderByDesc('release_date')->paginate(5);
        
        $languages = Language::all();
        $genres = Genre::all();
        return view('home',['movies'=>$movies,'languages'=>$languages,'genres'=>$genres]);
    }
}
