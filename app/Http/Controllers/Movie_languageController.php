<?php

namespace App\Http\Controllers;

use App\Movie_language;
use App\Movie;

use Illuminate\Http\Request;

class Movie_languageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $movie_id = Movie::find($request->movie_id)->id;
        $movie_language=new Movie_language();
        $movie_language->movie_id=$movie_id;
        $movie_language->language_id=$request->language_id;
        $movie_language->save();
        return redirect('/home')->with('success','Movie Language Added successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Movie_language  $movie_language
     * @return \Illuminate\Http\Response
     */
    public function show(Movie_language $movie_language)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Movie_language  $movie_language
     * @return \Illuminate\Http\Response
     */
    public function edit(Movie_language $movie_language)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Movie_language  $movie_language
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Movie_language $movie_language)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Movie_language  $movie_language
     * @return \Illuminate\Http\Response
     */
    public function destroy(Movie_language $movie_language)
    {
        //
    }
}
