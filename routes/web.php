<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/', 'DefaultController@index');
Route::post('/', 'DefaultController@index');

Route::post('/', 'SearchController@search')->name('movie.search');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/movie/add', 'MovieController@add')->name('movie.add');
Route::post('/movie/store', 'MovieController@store')->name('movie.store');
Route::post('/language/store', 'Movie_languageController@store')->name('language.store');
Route::get('/movie/{movie}', 'DefaultController@show')->name('movie');

Route::resource('movies','MovieController');
Route::resource('movie_genres','Movie_genreController');


